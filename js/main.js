
//Model objects
var tasksDetails = {
  get: function () { return document.querySelector('#taskDetails') },
  hide: function () { return document.querySelector('#taskDetails').style.display = "none"; },
  show: function () { return document.querySelector('#taskDetails').style.display = "flex"; },

  getNewTaskBtn: function () { return document.querySelector('#newTask') },
  getDeleteAllTaskBtn: function () { return document.querySelector('#deleteAll') },
  getNewCategoryBtn: function () { return document.querySelector('#newCategory') },
  getSearchInput: function () { return document.querySelector('#tsName') },

};

var taskArticle = {
  default: "none",
  hide: function () { return document.querySelector('#taskArticle').style.display = "none"; },
  show: function () { return document.querySelector('#taskArticle').style.display = "flex"; },

  get: function () { return document.querySelector('#taskArticle') },
  getCloseBtn: function () { return document.querySelector('#taskCloseBtn') },
  getSaveChgsBtn: function () { return document.querySelector('#taskSaveChgsBtn') },
  getTaskDesc: function () { return document.querySelector('#tsDescription') },
};

var categoryArticle = {
  default: "none",
  get: function () { return document.querySelector('#categoryArticle') },
  hide: function () { return document.querySelector('#categoryArticle').style.display = "none"; },
  show: function () { return document.querySelector('#categoryArticle').style.display = "flex"; },
  getCloseBtn: function () { return document.querySelector('#catCloseBtn') },
  getSaveChgsBtn: function () { return document.querySelector('#catSaveChgsBtn') },

};

// Actual models
var user = {
  name: 'Venkata Kalyanam',
  designation: 'Sr Full-stack developer'
};

var categories = [{
  name: 'IOS Development', taskList: [
    { name: 'Learn basics if IOS Swift', when: '02/03/2018', compvared: true },
    { name: 'Learn best practices of IOS Swift', when: '31/03/2018', compvared: false }
  ]
},
{
  name: 'Web Development', taskList: [
    { name: 'Learn basics if CSS', when: '01/02/2018', compvared: true },
    { name: 'Learn best practices of JavaScript', when: '31/03/2018', compvared: false }
  ]
}];

// Event handling using RxJS
var $documentOnLoad = Rx.Observable.fromEvent(window, 'load');

var $AddTask = Rx.Observable.fromEvent(tasksDetails.getNewTaskBtn(), 'click');
var $AddCategory = Rx.Observable.fromEvent(tasksDetails.getNewCategoryBtn(), 'click');
var $DeleteAllTask = Rx.Observable.fromEvent(tasksDetails.getDeleteAllTaskBtn(), 'click');

var $taskClose = Rx.Observable.fromEvent(taskArticle.getCloseBtn(), 'click');
var $taskSaveChgs = Rx.Observable.fromEvent(taskArticle.getSaveChgsBtn(), 'click');

var $categoryClose = Rx.Observable.fromEvent(categoryArticle.getCloseBtn(), 'click');
var $categorySaveChgs = Rx.Observable.fromEvent(categoryArticle.getSaveChgsBtn(), 'click');
var $taskCrossClose = Rx.Observable.fromEvent(document.querySelector('#taskCrossClose'), 'click');
var $categoryCrossClose = Rx.Observable.fromEvent(document.querySelector('#categoryCrossClose'), 'click');

$taskCrossClose.subscribe(
  function() {showTaskDetails()}
);

$categoryCrossClose.subscribe(
  function() {showTaskDetails()}
);

// utility functions
function profile() {
  // name element
  document.querySelector('#name').innerText = user.name;
  // work element
  document.querySelector('#work').innerText = user.designation;
};

function generateCategoryList() {
  var catEle = document.querySelector('#categories');

  // refresh content
  if (catEle.hasChildNodes()) {
    catEle.innerHTML = '';
  }

  categories.forEach(function(item) {
    var catItemEle = document.createElement('li');
    var cntSpan = document.createElement('span');
    cntSpan.className = 'task-count';
    cntSpan.innerText = item.taskList.length;
    catItemEle.innerHTML = item.name;
    catItemEle.appendChild(cntSpan);

    // main element
    catEle.appendChild(catItemEle);
  });
};

function generateTaskList() {

  var tbody = document.querySelector('#tasklist');

  // make sure it will not repeat same rows again
  if (tbody.hasChildNodes()) {
    tbody.innerHTML = '';
  }

  categories.forEach(function(item) {
    if (item.taskList && item.taskList.length > 0) {
      item.taskList.forEach(function(task) {
        var row = document.createElement('tr');

        var chkCol = document.createElement('td');
        var inputChk = document.createElement('input');
        var taskCol = document.createElement('td');
        var whenCol = document.createElement('td');

        inputChk.type = 'checkbox';
        inputChk.checked = task.compvared;
        chkCol.appendChild(inputChk);

        if (task.compvared) {
          taskCol.style.fontStyle = 'italic';
          taskCol.style.textDecorationLine = 'line-through';

          whenCol.style.fontStyle = 'italic';
          whenCol.style.textDecorationLine = 'line-through';
        }
        taskCol.innerHTML = task.name;
        whenCol.innerHTML = task.when;

        row.appendChild(chkCol);
        row.appendChild(taskCol);
        row.appendChild(whenCol);

        tbody.appendChild(row);
      });
    }
  });

};

function saveNewCategory() {

  var catName = document.querySelector('#category').value || '';
    if (catName) {
      categories.push({ name: catName, taskList: [] });
    }else {
      alert('Plz fill the fields');
    }

};

function saveNewTask() {
  var tsName = document.querySelector('#tsName').value || '';
    var when = document.querySelector('#when').value || '';
    if (tsName && when) {
      var cat = categories[categories.length - 1];
      cat.taskList.push({ name: tsName, when: when, compvared: false });
    }else {
      alert('Plz fill fields')
    }
};

function showTaskDetails() {
  tasksDetails.show();
  taskArticle.hide();
  categoryArticle.hide();

  // generate content of screen
  profile();
  generateCategoryList();
  generateTaskList();
};

function showNewTaskView() {
  tasksDetails.hide();
  taskArticle.show();
  categoryArticle.hide();
  // clear for new item to add
  document.querySelector('#tsName').value = '';
  document.querySelector('#when').value = '';
};

function showNewCategory() {
  tasksDetails.hide();
  taskArticle.hide();
  categoryArticle.show();
  document.querySelector('#category').value = '';
};

$taskClose.subscribe(
  function () {
    showTaskDetails();
  }
);

$taskSaveChgs.subscribe(
  function () {
    saveNewTask();
    showTaskDetails();
  }
);

$categoryClose.subscribe(
  function () { showTaskDetails() }
);

$categorySaveChgs.subscribe(
  function () {
    saveNewCategory();
    showTaskDetails();
  }
);

$documentOnLoad.subscribe(
  function () { showTaskDetails() }
);

$AddTask.subscribe(
  function () { showNewTaskView() }
);

$AddCategory.subscribe(
  function () { showNewCategory() }
);

$DeleteAllTask.subscribe(
  function () {
    categories.map(function (task) {
      task.taskList = [];
      showTaskDetails();
    });
  }
);
